# Getting started 

-----------------------

# Configure Environment LINUX machine

>  If necessary, go to Oracle Java SE Downloads, accept the license agreement, and download the installer for your distribution.
> 
> The Oracle JDK License has changed for releases starting April 16, 2019. See Java SE Development Kit 8 Downloads.
> Note: If installing the Oracle JDK in a cloud environment, download the installer to your local client, and then use scp (secure copy) to transfer the file to your cloud machines.

1. first check directory usr/lib/jvm exists, if not Make a directory for the JDK: 
        
          sudo mkdir -p /usr/lib/jvm

2. Extract the tarball and install the JDK:

          sudo tar zxvf jdk-version-linux-x64.tar.gz -C /usr/lib/jvm     

3. Tell the system that there's a new Java version available: 

          sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_version/bin/java" 1

4. Set the new JDK as the default: 

          sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_version/bin/java

5.  Verify the version of the JRE or JDK: 
      
          java -version

Output:          

      java version "1.8.0_202"
      Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
      Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)


          
## Configure MAVEN_HOME and JAVA_HOME
     ~$ echo 'export M2_HOME=/opt/apache-maven-3.8.6' >> ~/.bashrc
     ~$ echo 'export PATH=${M2_HOME}/bin:${PATH}' >> ~/.bashrc
     ~$ echo 'export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_202' >> ~/.bashrc
