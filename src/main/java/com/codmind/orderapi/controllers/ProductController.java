package com.codmind.orderapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codmind.orderapi.converters.ProductConverter;
import com.codmind.orderapi.dtos.ProductDTO;
import com.codmind.orderapi.entity.Product;
import com.codmind.orderapi.services.ProductService;
import com.codmind.orderapi.utils.WrapperResponse;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	@Autowired
	private ProductConverter productConverter;
	
	@GetMapping(value="/products/{productId}")
	public ResponseEntity<WrapperResponse<ProductDTO>> findById(@PathVariable("productId") Long productId) {
		Product product = productService.findById(productId);
		ProductDTO productDTO = productConverter.fromEntity(product);
		return new WrapperResponse<ProductDTO>(true, "success", productDTO)
				.createResponse(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/products/{productId}")
	public ResponseEntity<?> delete(@PathVariable("productId") Long productId) {
		productService.delete(productId);
		return new WrapperResponse<Object>(true, "success", null)
				.createResponse(HttpStatus.OK);
	}

	@GetMapping(value = "/products")
	public ResponseEntity<List<ProductDTO>> findAll(
			@RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "5") int pageSize){
		
		
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		
		List<Product> products = productService.findAll(pageable);
		
		List<ProductDTO> dtoProducts = productConverter.fromEntity(products);
		
		return new WrapperResponse(true, "Success", dtoProducts)
				.createResponse(HttpStatus.OK);		
	}

	@PostMapping(value = "/products")
	public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO) {
		Product newProduct = productService.save(productConverter.fromDTO(productDTO));
		
		ProductDTO productDTOResponse  = productConverter.fromEntity(newProduct);

		return new WrapperResponse(true, "Success", productDTOResponse)
				.createResponse(HttpStatus.CREATED);				
	}

	@PutMapping(value = "/products")
	public ResponseEntity<ProductDTO> update(@RequestBody ProductDTO productDTO) {
		Product exitsProduct = productService.save(productConverter.fromDTO(productDTO));
		
		ProductDTO productDTOResponse  = productConverter.fromEntity(exitsProduct);
		
		return new WrapperResponse(true, "Success", productDTOResponse)
				.createResponse(HttpStatus.OK);				
	}
}