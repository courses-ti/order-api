INSERT INTO products(name,price) VALUES('Product 1', 100);
INSERT INTO products(name,price) VALUES('Product 2', 200);
INSERT INTO products(name,price) VALUES('Product 3', 300);
INSERT INTO products(name,price) VALUES('Product 4', 400);
INSERT INTO products(name,price) VALUES('Product 5', 500);
INSERT INTO products(name,price) VALUES('Product 6', 600);
INSERT INTO products(name,price) VALUES('Product 7', 700);
INSERT INTO products(name,price) VALUES('Product 8', 800);
INSERT INTO products(name,price) VALUES('Product 9', 900);
INSERT INTO products(name,price) VALUES('Product 10', 1000);

INSERT INTO orders(reg_date, total) VALUES(now(), 1500);

INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,1, 100,1,100);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,2, 200,1,200);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,3, 300,1,300);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,4, 400,1,400);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,5, 500,1,500);

INSERT INTO orders(reg_date, total) VALUES(now(), 4000);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,6, 600,1,600);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,7, 700,1,700);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,8, 800,1,800);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,9, 900,1,900);
INSERT INTO order_lines(fk_order, fk_product, price, quantity, total) VALUES(1,10, 1000,1,1000);

INSERT INTO users(username,password) VALUES('Oscar', '1234');